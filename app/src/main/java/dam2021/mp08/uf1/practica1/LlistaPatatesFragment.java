package dam2021.mp08.uf1.practica1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class LlistaPatatesFragment extends Fragment {


    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<Patata> patates = new ArrayList<Patata>();
    private PatatesListRecyclerViewAdapter mAdapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View view = inflater.inflate(R.layout.fragment_llista_patates_list, container, false);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            this.mAdapter = new PatatesListRecyclerViewAdapter(this.patates, mListener);
            recyclerView.setAdapter(this.mAdapter);
        }

        // Inflate the layout for this fragment
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab_add = getActivity().findViewById(R.id.fab_add);
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LlistaPatatesFragment.this)
                        .navigate(R.id.action_LlistaPatatesFragment_to_AfegirPatataFragment);
                Snackbar.make(view, "Add premut", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab_add.show();
        FloatingActionButton fab_search = getActivity().findViewById(R.id.fab_search);
        fab_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LlistaPatatesFragment.this)
                        .navigate(R.id.action_LlistaPatatesFragment_to_CercaPatatesFragment);
                Snackbar.make(view, "Search premut", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab_search.show();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addInitialPatates();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void addInitialPatates() {
        //String dadesPatata = "ID \nTipus \nSembra \nRecollida \n";
        Patata p = new Patata("1","patata","una patata","1");
        Patata p2 = new Patata("2","patata2","dos patates","2");
        Patata p3 = new Patata("3","patata3","tres patates","3");
        Patata p4 = new Patata("4","patata4","quatre patates","4");
        patates.add(p);
        patates.add(p2);
        patates.add(p3);
        patates.add(p4);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Patata item);
    }


}