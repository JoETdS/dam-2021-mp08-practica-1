package dam2021.mp08.uf1.practica1;

import dam2021.mp08.uf1.practica1.LlistaPatatesFragment.OnListFragmentInteractionListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PatatesListRecyclerViewAdapter extends RecyclerView.Adapter<PatatesListRecyclerViewAdapter.ViewHolder> {

    private final List<Patata> llistaPatates;
    private final OnListFragmentInteractionListener mListener;

    public PatatesListRecyclerViewAdapter(List<Patata> items, OnListFragmentInteractionListener listener) {
        llistaPatates = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_llista_patates, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = llistaPatates.get(position);
        holder.mIdView.setText(llistaPatates.get(position).getNom());
        holder.mNomView.setText(llistaPatates.get(position).getNom());
        holder.mDescripcioView.setText(llistaPatates.get(position).getDescripcio());
        holder.mValorView.setText(llistaPatates.get(position).getValor());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return llistaPatates.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mNomView;
        public final TextView mDescripcioView;
        public final TextView mValorView;
        public Patata mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.patata_id);
            mNomView = (TextView) view.findViewById(R.id.patata_nom);
            mDescripcioView = (TextView) view.findViewById(R.id.patata_descripcio);
            mValorView = (TextView) view.findViewById(R.id.patata_valor);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNomView.getText() + "'";
        }
    }
}
