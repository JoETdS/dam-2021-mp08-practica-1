package dam2021.mp08.uf1.practica1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class CercaPatatesFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cerca_patates, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.boto_enrere).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(CercaPatatesFragment.this)
                        .navigate(R.id.action_CercaPatatesFragment_to_LlistaPatatesFragment);
            }
        });

        FloatingActionButton fab_search = getActivity().findViewById(R.id.fab_search);
        fab_search.hide();
        FloatingActionButton fab_add = getActivity().findViewById(R.id.fab_add);
        fab_add.show();
    }
}